#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of py-rtb-jobs.
# https://github.com/someuser/somepackage

# Licensed under the MIT license:
# http://www.opensource.org/licenses/MIT-license
# Copyright (c) 2018, NirD Shabo <nird@nrdlabs.com>
from datetime import datetime
from elasticsearch import Elasticsearch
import os
import jq

import pandas as pd
import json
import sys
import itertools
import json
class EsIngestor():
    # es = None
    # config = None
    def __init__(self):
        json_file = open('config.json').read()
        self.config = json.loads(json_file)[os.environ.get("ENV","development")]
        self.es = Elasticsearch(self.config.get("es_hosts"),http_compress = True)


    def query(self,type="dsp"):
        esqueryObject=self.config['searchQuery'][type]
        self.es.search(esqueryObject)
