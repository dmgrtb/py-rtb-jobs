#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of py-rtb-jobs.
# https://github.com/someuser/somepackage

# Licensed under the MIT license:
# http://www.opensource.org/licenses/MIT-license
# Copyright (c) 2018, NirD Shabo <nird@nrdlabs.com>
from datetime import datetime
import time
import progressbar
# from elasticsearc import Elasticsearchs
import os
import pandas as pd
import json
import sys
import itertools
from es_report_aggregator import EsReportAggregator
from dateutil import parser as dateparser
import dataset
from pandas.io.json import json_normalize


class AggregationJobs():
    # es = None
    # config = None
    def __init__(self):
        json_file = open('config.json').read()
        self.env=os.environ.get("ENV","development")
        print("ENV " + self.env)
        self.config = json.loads(json_file)[self.env]
        self.config["table_unique_idx"] = dict()
        self.config["table_unique_idx"]["dsp"] = ['date', 'publisher', 'supplysource', 'domain', 'app', 'appid', 'advertiser', 'campaign', 'creative', 'creativeid', 'algorithm', 'department', 'adtype', 'device', 'os', 'connectiontype', 'carrier', 'iabcat', 'country']
        self.config["table_unique_idx"]["ssp"] = ['date', 'publisher', 'supplysource', 'domain', 'app', 'appid', 'advertiser', 'campaign', 'creative', 'creativeid', 'algorithm', 'department', 'adtype', 'device', 'os', 'connectiontype', 'carrier', 'iabcat', 'country']
        self.config["table_unique_idx"]["xml"] = ['date', 'xdemandpartner', 'xdemanddeal', 'xcampaign', 'xsupplypartner', 'xsupplysource', 'xos', 'xbrowser', 'xcountry', 'xdomain', 'xblockreason', 'xsubid', 'xkeywords']
        self.db = dataset.connect(self.config["postgres"]["rtb_aggregation_uri"])

    def query(self,ds,de,index,tableName):
        results = list()
        self.elastic_aggregator = EsReportAggregator(self.config, ds, de, index, tableName)
        self.elastic_aggregator.execute_query_in_serial(results)
        return [self.align_record_data(x) for x in results]

    def align_record_data(self,record):
        arr = set(record.keys())
        if "device" not in arr:
            record["device"] = "NA"
        if "iabcat" not in arr:
            record["iabcat"] = "NA"
        for k in arr:
            if record.get(k) is "na":
                record[k] = "NA"
        return record

    def upsert_db(self, tableName, records):
        table = self.db.load_table(tableName)
        # r = table.insert_many(records.to_dict())
        # print(r)
        idx_keys = self.config["table_unique_idx"].get(tableName,None)
        arlen = len(records)-1
        print("DB INSERTION STARTED!")
        with progressbar.ProgressBar(max_value=arlen) as bar:
            for idx,record in records.iterrows():
                r = record.to_dict()
                if idx_keys is None:
                    idx_keys=r.keys()
                db_res = table.upsert(r, idx_keys)
                bar.update(idx)




def __main__():
    args =  sys.argv
    ds = None
    de = None
    index = None
    tableName = None
    import timeit
    for arg in args:
        if '=' in arg:
            args_arr=arg.split("=")
            if "ds" in args_arr[0] or "date_start" in args_arr[0] or "date-start" in args_arr[0]:
                # ds = datetime.strptime(args_arr[1],"%Y-%m-%d")
                ds = dateparser.parse(args_arr[1].strip())
            if "--de" in args_arr[0] or "--date_end" in args_arr[0] or "--date-end" is args_arr[0]:
                # de = datetime.strptime(args_arr[1].strip().lower(),"%Y-%m-%d")
                de = dateparser.parse(args_arr[1].strip())
            if "--indexname" in args_arr[0].lower().strip():
                index = args_arr[1].strip()
            if "--tableName" in args_arr[0].lower().strip():
                tableName = args_arr[1].strip()
    if index is not None and tableName is None:
        tableName = index.split("-")[0]
    print(str(ds.isoformat()) + " " + str(de.isoformat()) + " " + str(index) + " table = " + tableName)
    agg_jobs = AggregationJobs()
    res = json_normalize(agg_jobs.query(ds, de, index, tableName))
    # print(res.head())
    t1 = time.time()
    agg_jobs.upsert_db(tableName, res)
    t2 = time.time()
    print("insert to db Time=%s" % (t2-t1))

__main__()
