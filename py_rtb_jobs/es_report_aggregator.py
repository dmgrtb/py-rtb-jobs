#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of py-rtb-jobs.
# https://github.com/someuser/somepackage

# Licensed under the MIT license:
# http://www.opensource.org/licenses/MIT-license
# Copyright (c) 2018, NirD Shabo <nird@nrdlabs.com>

from elasticsearch import Elasticsearch
from datetime import datetime,timedelta
import pandas as pd
import json
import sys
import os
import itertools
import progressbar
# import



class EsReportAggregator():

    def __init__(self, config, ds, de, indexName="dsp-2018.*",tableName="dsp"):
        self.environment = config
        self.config = config
        self.base_request = self.environment['searchQuery'].get(tableName,None)
        if self.base_request is None:
            raise "Non valid table name = "+tableName
        self.indexName = indexName
        self.es = Elasticsearch(self.environment["es_hosts"],http_compress=True)
        self.date_start = ds
        self.date_end = de
        self.date_range = []
        self.current_indexName = indexName
        self.tableName=tableName
        current_date = self.date_start
        while current_date<=self.date_end:
            self.date_range.append(current_date.isoformat())
            current_date=(current_date  + timedelta(hours=1))

    def print_json(self,resDict=dict()):
        indented_json = json.dumps(resDict, sort_keys=True, separators=(',', ': '), indent=4,
                                   ensure_ascii=False)
        print(indented_json)

    def deep_parse(self,ser):
        arra=list()
        for t in ser:
            z = dict(zip(*[t[0], t[1]]))
            d = dict(t[2])
            for k in t[2].keys():
                z[k] = t[2].get(k)
            arra.append(z)
        # print(self.print_json(arra[0]))
        return arra

    def process_agg(self, bucket, indexes=(), names=()):
        """
        Recursively extract agg values
        :param bucket: a bucket contains either sub-buckets or a bunch of aggregated values
        :return: a list of tuples: (index_name, index_tuple, row)
        """
        # for each agg, yield a row
        row = {}
        for k, v in bucket.items():
            if isinstance(v, dict):
                if 'buckets' in v:
                    for sub_bucket in v['buckets']:

                        if 'key_as_string' in sub_bucket:
                            key = sub_bucket['key_as_string']
                        else:
                            key = sub_bucket['key']
                        for x in self.process_agg(sub_bucket, indexes + (key,),
                                                  names + (k,)):
                            yield x
                elif 'value' in v:
                    row[k] = v['value']
                elif 'values' in v:  # percentiles
                    row = v['values']
                else:
                    row.update(v)  # stats
            else:
                if k == 'doc_count':  # count docs
                    pass

        if len(row) > 0:
            yield (names, indexes, row)

    def execute_query_in_serial(self, proccsed_array):
        i = 0
        arlen = len(self.date_range)-1
        print("ELASTIC DATA GATHERING STARTED for %s - %s!" % (self.date_range[0], self.date_range[len(self.date_range)-1]))
        with progressbar.ProgressBar(max_value=arlen) as bar:
            for _date in self.date_range:
                i = i+1
                bar.update(i)
                if i ==len(self.date_range)-1:
                    return
                nextDate = self.date_range[i]
                timeRange = {"gte":_date,"lt":nextDate};
                searchParams = self.base_request
                searchParams["doc_type"] = self.tableName
                searchParams["type"] = self.tableName
                searchParams["index"] = self.current_indexName
                searchParams["size"]=0
                searchParams["body"]["query"]["bool"]["must"][0]["range"]['@timestamp'] = timeRange
                results = self.es.search(params=searchParams,body=searchParams["body"],analyze_wildcard=True,timeout="30m",request_timeout=180)
                proccsed_array += self.deep_parse(self.process_agg(results["aggregations"]))
