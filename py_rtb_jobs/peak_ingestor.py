#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of py-rtb-jobs.
# https://github.com/someuser/somepackage

# Licensed under the MIT license:
# http://www.opensource.org/licenses/MIT-license
# Copyright (c) 2018, NirD Shabo <nird@nrdlabs.com>
from datetime import datetime,timedelta
import time
# import timedelta
import progressbar
# from elasticsearc import Elasticsearchs
import os
from datetime import datetime,timedelta
import pandas as pd
import numpy as np
import json
import sys
import requests
from pandas.io.json import json_normalize
from dateutil import parser as dateparser

class PeakIngestor():

    def __init__(self, date_start, date_end):
        self.fields_querystring = "fields=peak_static.dimdate_gmt.date_string&fields=peak_static.dimdate_gmt.hour&fields=domain&fields=publisher_bundle&fields=browser&fields=os&fields=os_version&fields=device_type&fields=device_model&fields=pub_network_id&fields=publisher_id&fields=advertiser_id&fields=placement_id&fields=peak_static.deals.name&fields=peak_static.campaigns.title&fields=line_item_id&fields=peak_static.campaigns.environment&fields=creative_size&fields=pricing_amount&fields=pricing_type&fields=adv_network_cost&fields=advertiser_network_revenue&fields=profit&fields=impressions&fields=view_rate&fields=ecpm&fields=revenue_ecpm&fields=view_ecpm"
        self.filter_querystring = "filter_pricing_type=eq%7COCPM&auth=914eaa9b83614dc3a379410e5ed00a8c"
        self.query_uri = "https://dmg.peak226.com/v1/report/performance?dateRange=%s-%s&%s&%s" % (date_start,date_end,self.fields_querystring,self.filter_querystring)
        self.base_api_uri  = "https://dmg.peak226.com/%s?auth=914eaa9b83614dc3a379410e5ed00a8c"
        self.df = None
        self.raw_data = dict()
        self.peak_data = dict()
        self.peak_data['auth_token'] = "914eaa9b83614dc3a379410e5ed00a8c"
        self.peak_data['network_name'] = 'dmg'

    def execute_query(self):
        url = self.query_uri
        print(url)
        resp = requests.get(url=url)
        data = resp.json()
        # print(repr(data))
        print("data length: " + str(len(data.get("data"))))
        return self.parse(data.get("data"))

    def parse(self, data):
        inde = data[1:]
        ke=tuple(data[0])
        da=list()
        for value in inde:
            n=dict()
            i = 0
            for k in ke:
                n[k] = value[i]
                i=i+1
            n["datetime"] = (dateparser.parse(n.get("peak_static.dimdate_gmt.date_string")) + timedelta(hours=n.pop("peak_static.dimdate_gmt.hour")))
            view_rate=n.get("view_rate",0)
            n["requests"] =n.get("impressions",0)
            if view_rate is not None:
                n["requests"] = round(n.get("impressions",0)/view_rate,0)
            da.append(n)

        self.df = pd.DataFrame(data=da).groupby(by=["line_item_id","datetime"],as_index=False).filter(lambda g: len(g) > 22)
        df = self.df.groupby(["line_item_id","pricing_type"],as_index=False).agg({"impressions":lambda x: np.sum(x),"requests":lambda x: np.sum(x)}).assign(fill_rate = lambda de: de.impressions/de.requests)
        return df.T.to_dict().values()

    def split_bid(self, line_item_id, line_item):
        url = self.base_api_uri % ("v1/demanditems/"+str(line_item_id))
        print(url)
        r=requests.get(url=url)
        original_item = r.json()
        unit_rate = float(original_item["unitRate"])
        start_rate = unit_rate
        if unit_rate > 0.1:
            start_rate= unit_rate - 0.1
        end_rate= unit_rate+0.1
        print("original unit rate = " + str(unit_rate))
        bid_range = np.arange(start_rate,end_rate,0.02)
        results=[]
        update_url = url = self.base_api_uri % ("v1/demanditems")
        for rate in bid_range:
            if rate != unit_rate:
                new_data_item=dict(original_item)
                new_data_item["unitRate"] = rate
                name =new_data_item["name"] + "_RATE_"+str(rate)
                new_data_item["name"] =name
                new_data_item.pop("created")
                new_data_item.pop("modified")
                new_data_item.pop("modifiedBy")
                new_data_item.pop("createdBy")
                try:
                    new_res=requests.post(update_url,json=new_data_item)
                    res_data=new_res.json()
                    print(str(rate) + " = " + repr(res_data))
                    new_data_item["id"]=res_data[u'id']
                except Exception as e:
                    print(e)
                    new_data_item["results"]=e.message
                results.append(new_data_item)
        print(results)
        return results

    def close_line_item(self, line_item_id, line_item):
        url = self.base_api_uri % ("v1/demanditems/"+str(line_item_id))
        print(url)
        # r=requests.get(url=url)
        # print()
        original_data=line_item
        original_data['status'] = u'Inactive'
        resp = requests.post(url=url, json=original_data)
        data = resp.json()
        # print(json.dumps(data))
        return resp.status_code

    def update_line_item(self,line_item_id,line_item):
        url = self.base_api_uri % ("v1/demanditems/"+str(line_item_id))
        print(url)
        resp = requests.post(url=url, json=line_item)
        data = resp.json()
        return resp.status_code

    def ruler(self, data):
        new_bids = list()
        for row in data:
            row["actions"] = []
            row["results"] = []
            print(repr(row))
            line_item_id=row["line_item_id"]
            if row["pricing_type"] == "OCPM" and row["impressions"] > 3000:
                print "split bid"
                status_results=self.split_bid(row["line_item_id"], row)
                new_bids = new_bids + status_results
                row["results"]=status_results
                row["actions"] = ["SPLIT BID","MOVE TO CPV"]
                print "move cmp of line_item_id " + str(row["line_item_id"]) + "to cpv "
                adata=dict()
                adata["ratePriceType"] = "CPV"
                statusRes = self.update_line_item(line_item_id,adata)
                row["results"].append(statusRes)
            elif row["impressions"]>=1000 and row["impressions"]<3000  and row["fill_rate"]<=0.2:
                print "close cmp of line_item_id " + str(row["line_item_id"])
                row["actions"] = ["CLOSE CMP"]
                rdata=dict()
                rdata["line_item_id"] = row["line_item_id"]
                rdata["status"] = 'Inactive'
                status=self.close_line_item(row["line_item_id"], rdata)
                row["results"] = [status]
            elif row["impressions"] < 1000 and row["requests"] > 10000:
                print "close cmp of line_item_id " + str(row["line_item_id"])
                row["actions"] = ["CLOSE CMP"]
                rdata = dict()
                rdata["line_item_id"] = row["line_item_id"]
                rdata["status"] = 'Inactive'
                status = self.close_line_item(row["line_item_id"], rdata)
                row["results"] = [status]
        return (new_bids,data)

        # d.to_excel("results.xlsx")
def __main__():
    args =  sys.argv
    ds = None
    de = None
    for arg in args:
        if '=' in arg:
            args_arr=arg.split("=")
            if "ds" in args_arr[0] or "date_start" in args_arr[0] or "date-start" in args_arr[0]:
                # ds = datetime.strptime(args_arr[1],"%Y-%m-%d")
                ds = dateparser.parse(args_arr[1].strip()).strftime("%Y-%m-%d")
            if "--de" in args_arr[0] or "--date_end" in args_arr[0] or "--date-end" is args_arr[0]:
                # de = datetime.strptime(args_arr[1].strip().lower(),"%Y-%m-%d")
                de = dateparser.parse(args_arr[1].strip()).strftime("%Y-%m-%d")
    ingestor = PeakIngestor(ds,de)
    data= ingestor.execute_query()

    results = ingestor.ruler(data)
    newdf=pd.DataFrame(results[0])
    datadf=pd.DataFrame(results[1])
    print(datadf.head())
    newdf.to_csv("new_bids_%s.csv" % (str(datetime.now().isoformat())))
    datadf.to_csv("results_%s.csv" % (str(datetime.now().isoformat())))


__main__()
