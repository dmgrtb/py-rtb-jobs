#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of py-rtb-jobs.
# https://github.com/someuser/somepackage

# Licensed under the MIT license:
# http://www.opensource.org/licenses/MIT-license
# Copyright (c) 2018, NirD Shabo <nird@nrdlabs.com>

from preggy import expect

from py_rtb_jobs import __version__
from tests.base import TestCase


class VersionTestCase(TestCase):
    def test_has_proper_version(self):
        expect(__version__).to_equal('0.1.0')
